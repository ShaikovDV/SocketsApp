#include "SocketTransfer.h"
#include <process.h>

int CSocketTransfer::WSAcount = 0;

CSocketTransfer::CSocketTransfer(const CSocketTransfer& ST)
{ 
		ErrorMessage = "����������� ������� ���������!";
		ErrorCode = 1;

        return;
}

// ����������� ��� �������� ���������� ������, ������� ����� ������� ����������� �� ���������� ����� TCP
CSocketTransfer::CSocketTransfer(char* port, // ���� TCP ��� �����������
	int bufferSize, // ������ ������, ������� ����� �������.
	void (*ConnectionRecieved)(CSocketTransfer* ST, int ConnIdentifier), // ������ �� �����, ������� ����� ���������� ��� ����� �����������. ���� �� ����, �������� nullptr.
	void (*DataReceived)(CSocketTransfer* ST, int ConnIdentifier, char* Data), // ������ �� �����, ������� ����� ���������� ��� ����� ������ �� �������. ���� �� ����, �������� nullptr.
	void (*ConnectionClosed)(CSocketTransfer* ST, int ConnIdentifier)) // ����� �� �����, ������� ����� ���������� ��� ������� ����������� �� ������� �������. ���� �� ����, �������� nullptr.
{
	// ��� �������� ���������� �������� � ���������.
	// ��������� �������� ���������� ������ ������ ����� �� �������� ���������� �������.
	WSAcount++;

	// �������������� �����
	recvbuflen = bufferSize;
	recvbuf = new char[bufferSize+1];
	recvbuf[bufferSize]=0;
	// �������������� Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0)
	{
        ErrorMessage = "��������� ������ ��� �������. ������ �� ���������������.";
		ErrorCode = iResult;

        return;
    }

	addrinfo *result = NULL;
	addrinfo hints;

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    // ���������� ��������� ����� � ����
    iResult = getaddrinfo(NULL, (PCSTR)port, &hints, &result);
    if ( iResult != 0 )
	{
        ErrorMessage = "��������� ���������� �� ������ ����������� �������.";
		ErrorCode = iResult;

        return;
    }

    // ������ �������������� �����, � �������� ����� ������������ ��� �������
    SOCKET ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (ListenSocket == INVALID_SOCKET)
	{
        ErrorMessage = "�������� ������ ����������� �������.";
		ErrorCode = WSAGetLastError();

        freeaddrinfo(result);

        return;
    }

    // ����������� ����� � ������
    iResult = bind( ListenSocket, result->ai_addr, (int)result->ai_addrlen);
    if (iResult == SOCKET_ERROR)
	{
        ErrorMessage = "������������� ������ ����������� �������.";
		ErrorCode = WSAGetLastError();

        freeaddrinfo(result);
        closesocket(ListenSocket);

        return;
    }

    freeaddrinfo(result);

	// ��������� ������������� �����
    iResult = listen(ListenSocket, SOMAXCONN);
    if (iResult == SOCKET_ERROR)
	{
        ErrorMessage = "������������� ������ ����������� �������.";
		ErrorCode = WSAGetLastError();
        closesocket(ListenSocket);
        return;
    }

	// �.�. ���� ����� � ������ ������, ���������� �������� � ���� ��� ����������� ������
	// ��� ����� ������ � �������������� ���������
	SocketData* SD = new SocketData();

	SD->identifier = socketsMas.size(); // ������� ����� ������� ������� � ������������� ��������������� ������
	SD->socketsMas = &socketsMas; // 
	socketsMas.push_back(ListenSocket); // ��������� � ������ �����
	SD->buf = this->recvbuf; // ������� ����� ������ ��� �����
	SD->BufLength = this->recvbuflen; // ����� ������
	SD->CRecieved = ConnectionRecieved; // ������ �� ����� ��� ������ ��� ����� �����������
	SD->DataReceived = DataReceived; // ������ �� ����� ��� ������ ��� ��������� ������
	SD->CClosed = ConnectionClosed; // ������ �� ����� ��� ������ ��� �������� ����������� �����������
	SD->DataReceivedCl = nullptr; // ��� ������� �� ������������
	// ������� ������ �� ������� ������. ������ ����� ������������ �� ��� ������.
	// ��� ���������� ��� ����, ����� ����� ���� ��������� ����������� ���� ������ ���������� ��������
	SD->ST = this; 

	// ��������� ���� � ������ ������
	_beginthread(AcceptSocketConnection, 0, SD);
	
	// ���������� ��������, ������ ���������� ��� �� ���������
	ErrorCode=0;
	ErrorMessage=nullptr;
}

// ����������� ��� ����������� ������, ������� ����� ����� ������������ �� ��������� ������ � �����
CSocketTransfer::CSocketTransfer(char* host, // ����� �������
	char* port, // ���� �������
	int bufferSize, // ������ ����������� ������.
	void (*DataReceived)(CSocketTransfer* ST, char* Data)) // ������ �� �����, ������� ����� ���������� ��� ����� ���������� �� �������
{
	// ��� �������� ���������� �������� � ���������.
	// ��������� �������� ���������� ������ ������ ����� �� �������� ���������� �������.
	WSAcount++;

	// �������������� �����
	recvbuflen = bufferSize;
	recvbuf = new char[bufferSize+1];
	recvbuf[bufferSize]=0;
	
	WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                    *ptr = NULL,
                    hints;

    int iResult = 0;

    // �������������� Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0)
	{
        ErrorMessage = "��������� ������ ��� �������. ������ �� ���������������.";
		ErrorCode = iResult;
		
        return;
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // �������� ���������� �� ������ � ����� �������
    iResult = getaddrinfo(host, port, &hints, &result);
    if ( iResult != 0 )
	{
        ErrorMessage = "��������� ���������� �� ������ ����������� �������.";
		ErrorCode = iResult;
        return;
    }

    // �������� ������������, ���� �� ���������
    for(ptr=result; ptr != NULL ;ptr=ptr->ai_next)
	{
        // ������ ����� �����
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET)
		{
            ErrorMessage = "�������� ������ ����������� �������.";
			ErrorCode = iResult;
            return;
        }

		// ������������ � �������
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR)
		{
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET)
	{
        ErrorMessage = "����������� � ������� ���������� �������.";
		ErrorCode = iResult;
        return;
    }

	// ��������� � ������ ������� ���������� �����
	socketsMas.push_back(ConnectSocket);
	
	//��� �������� � ������ ����� ������ ���������
	SocketData* SD = new SocketData();

	SD->identifier = 0; // ��� ����������� ������ ������ 0
	SD->buf = this->recvbuf; // ������ �� �����
	SD->BufLength = this->recvbuflen; // ����� ������
	SD->CRecieved = nullptr; // �� ������������ ��� ����������� ������
	SD->DataReceived = nullptr; // �� ������������ ��� ����������� ������
	SD->CClosed = nullptr; // �� ������������ ��� ����������� ������
	SD->DataReceivedCl = DataReceived; // ������ �� �����, ���������� ��� ��������� ������ �� �������
	SD->socketsMas = &socketsMas; // ������ �� ������
	SD->ST = this; // ������ �� ������� ������

	// ��������� � ������ ������
	_beginthread(ReceiveData, 0, SD);
	
	ErrorCode=0;
	ErrorMessage=nullptr;
}

// ����� ��� ������ � ������ ������ ��������� �������� ����������� ������ �������
void CSocketTransfer::AcceptSocketConnection(void* SDAddr)
{
	// ������������� ������
	SocketData SD = *(SocketData*)SDAddr;

	// ������������� ����� �� �������
	SOCKET ListenSocket = (*SD.socketsMas)[SD.identifier];

	// ���� �� ����� ������� ����������� �� ������������ ���������� ������������� �����������
	// ������� �� �� ������������, ������������ ���� ��� �������� ������ �����������
	while(true)
	{
		// ������� ����������� ����������� ������
		SOCKET ClientSocket = accept(ListenSocket, NULL, NULL);
		if (ClientSocket == INVALID_SOCKET)
		{
			// ���� ���������� ��������
			break;
		}

		// ��������� ������� ������� � ������ �����
		// ��� ����� ������ ��������� � ������� � �� ��� ����������� ������

		SocketData* SDRec = new SocketData();
		SDRec->socketsMas = SD.socketsMas;
		SDRec->identifier = SD.socketsMas->size();
		SDRec->socketsMas->push_back(ClientSocket); // ��������� � ������ ����� �����
		SDRec->buf = SD.buf;
		SDRec->BufLength = SD.BufLength;
		SDRec->CRecieved = SD.CRecieved;
		SDRec->DataReceived = SD.DataReceived;
		SDRec->DataReceivedCl = nullptr;
		SDRec->CClosed = SD.CClosed;
		SDRec->ST = SD.ST;

		// ����� ������ ���������� �������
		Sleep(500);

		// ���� ����� ����� ��� ������
		if(SD.CRecieved != nullptr)
			// ������� ������ �� ������� ������ � ����� ������ � ������� � �������� ��� ��������������
			SD.CRecieved((CSocketTransfer*)SD.ST, SDRec->identifier);

		// ��������� �������� ����������� ���������� � ����� ������
		_beginthread(ReceiveData, 0, SDRec);
	}
	// ���� ��������� �����������, �� �������� ������� ��
	delete SDAddr;
	_endthread();
}

// ����� ��� �������� ����� ������ � ��������� ������
// ��������!! ����� ������ ����������� ������ ������ � ������� ����� �����!!!
void CSocketTransfer::ReceiveData(void* SDAddr)
{
	// ������������� ���������
	SocketData SD = *(SocketData*)SDAddr;

	// ������������� ����� �� ���������
	SOCKET ClientSocket = (*SD.socketsMas)[SD.identifier];
	int iResult=0;

	std::string buffer;

	do
	{
		// ������� ����
		iResult = recv(ClientSocket, SD.buf, SD.BufLength, 0);
		if (iResult > 0)
		{
			SD.buf[iResult]=0;

			buffer += SD.buf;

			size_t StrEnd = buffer.find("</text>", 0);

			while(StrEnd != std::string::npos)
			{
				char* text = new char[StrEnd-5];
				text[StrEnd-6]=0;
				
				memcpy(text, buffer.substr(6, StrEnd-6).c_str(), StrEnd-6);

				// �������� ������, ���� �� ��������
				if(SD.DataReceived != nullptr)
					SD.DataReceived((CSocketTransfer*)SD.ST, SD.identifier, text);
				if(SD.DataReceivedCl != nullptr)
					SD.DataReceivedCl((CSocketTransfer*)SD.ST, text);

				delete[] text;

				buffer = buffer.substr(StrEnd+7);

				StrEnd = buffer.find("</text>", 0);
			}
		}
		else  if(iResult<0)
		{
			// ������
		}

	} while (iResult > 0);

	// ���� ����� �����, ��������
	if(SD.CClosed != nullptr)
		SD.CClosed((CSocketTransfer*)SD.ST, SD.identifier);

	// �������� ����� � �������
	(*SD.socketsMas)[SD.identifier]=0;

	// ������� ��, ��� ����������
	delete SDAddr;
	closesocket(ClientSocket);
	_endthread();
}

// ������� ������ � ������� � ������� �� �������������� ����������� ������
void CSocketTransfer::Send(int ConnIdentifier, const char* data)
{
	// ������ ������ �����
	SOCKET ClientSocket = socketsMas[ConnIdentifier];
	
	// ���� �� ��� ��� ������, ������������
	if(ClientSocket == 0) return;

	// �������� ������

	std::string data_text = "<text>";
	data_text += data; // ���� ������� ��� � ������ ��� ����� <text></text>
	data_text += "</text>";

	int iSendResult = send( ClientSocket, data_text.c_str(), data_text.length(), 0 );
	if (iSendResult == SOCKET_ERROR)
	{
		ErrorMessage = "�������� ������ ����������� �������. ������� ���������� ����� �������� � ��������� ������.";
		ErrorCode = WSAGetLastError();
		closesocket(ClientSocket);
		socketsMas[ConnIdentifier] = 0;
		return;
	}
}

// �������� ������ �� ������� �� ������
void CSocketTransfer::Send(const char* data)
{
	Send(0, data);
}

// ����������
CSocketTransfer::~CSocketTransfer(void)
{
	// ��������� �����������
	CloseConnection();
	// ��������� ���������� ������� ��������
	WSAcount--;
	
	// ������� �����
	delete[] recvbuf;

	// ���� �������� � ��������� �� ��������, ���������� ��������
	if(WSAcount == 0)
		WSACleanup();
}

// ����� ��� �������� ���� �� �������� �������
void CSocketTransfer::CloseConnection()
{
	for(size_t i=0; i<socketsMas.size(); i++)
	{
		if(socketsMas[i] != 0)
		{
			closesocket(socketsMas[i]);
			socketsMas[i] = 0;
		}
	}
}

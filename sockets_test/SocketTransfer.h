#pragma once

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <deque>

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

class CSocketTransfer
{
public:
	
	// ����������� ��� "����������" ������, ��������������� ��������� ���� � ������������ ����������� �� ��������
	CSocketTransfer(char* port, int bufferSize, 
		void (*ConnectionRecieved)(CSocketTransfer* ST, int ConnIdentifier), 
		void (*DataReceived)(CSocketTransfer* ST, int ConnIdentifier, char* Data), 
		void (*ConnectionClosed)(CSocketTransfer* ST, int ConnIdentifier));

	// ����������� ��� "�����������" ������. ����� ������������ � ���������� �������:�����.
	CSocketTransfer(char* host, char* port, int bufferSize, void (*DataReceived)(CSocketTransfer* ST, char* Data));

	// ���������� �����������. ������ ��� ���������� ����������� �������.
	CSocketTransfer(const CSocketTransfer& ST);

	~CSocketTransfer(void);

	char* ErrorMessage;
	int ErrorCode;

	// ����� ��� �������� ������ �� ������� � ���������� ������� (�� ��������������)
	void Send(int ConnIdentifier, const char* data);
	// ����� ��� �������� ������ �� ������� � �������
	void Send(const char* data);

	// ����� ��������� ��� �����������
	void CloseConnection();

    int recvbuflen;

private:
	WSADATA wsaData;
    int iResult;

	// ����� ��� ����� ������
    char* recvbuf;

	// ����� ��� ����� ����� �����������
	static void AcceptSocketConnection(void*);
	// ����� ��� ��������� ������
	static void ReceiveData(void*);

	// ������ ��� �������� �������
	std::deque<SOCKET> socketsMas;

	// ������ ��� �������� ���� �������� ����� ������ � ���������.
	// ����� ����� �������� ���������� ������ 0 ����������� ������� WSACleanup
	static int WSAcount;
};

// ��������� ��� �������� ������ ������ � ������ ������ ����� _beginthread
struct SocketData
{
	int BufLength;
	char* buf;
	void (*CRecieved)(CSocketTransfer* ST, int ConnIdentifier);
	void (*DataReceived)(CSocketTransfer* ST, int ConnIdentifier, char* Data);
	void (*DataReceivedCl)(CSocketTransfer* ST, char* Data);
	void (*CClosed)(CSocketTransfer* ST, int ConnIdentifier);
	std::deque<SOCKET>* socketsMas;
	int identifier;
	void* ST;
};


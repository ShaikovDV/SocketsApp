#include "SocketTransfer.h"
#include "Telemetry.h"
#include <iostream>
#include <time.h>
//#include <sys/types.h>
#include <sys/timeb.h>
#include <sstream>
#include <ctime>


bool end=false;

// ������ ������ ��� ��������� ������� ����������� � ������� �������
void ConnectionReceived(CSocketTransfer* CST, int CI)
{
	CST->Send(CI, "HELL-O\r\n");
	
	printf("New Connection Identifier: %ld\n", CI);
}

// ������ ������ ��� ��������� ������� ���������� ������� �� �������
void ConnectionClosed(CSocketTransfer* CST, int CI)
{
	printf("Connection Closed: %ld\n", CI);
}

// ������ ������ ��� ��������� ������� ����� ������ �� ������� ��������
void DataRec(CSocketTransfer* CST, int identifier, char* data)
{
	if(strlen(data)==2)
		end=true;

	std::cout<<"Server Received from "<<identifier<<": "<<data<<std::endl;

	//CST->Send(identifier, "OK");
}

// ������ ������ ��� ��������� ������� ����� ������ �� ������� ��������
void DataRec(CSocketTransfer* CST, char* data)
{
	if(strlen(data)==2)
		end=true;

	std::cout<<"Client Received: "<<data<<std::endl;

	//CST->Send(data);
}

int __cdecl main(void) 
{
	// ������ ��������� �����, �������������� ���� 8888
	// ������ ����� ���� ����� ��� � ������ ���������� ������ ��� ��������� �������� ���� ������������ ��������
	CTelemetrySocket* ST = new CTelemetrySocket("8888", ConnectionReceived, DataRec, ConnectionClosed);
	
	// ������ ���������� �����, �������������� � ���������� ����� 8888
	// ������ ����� ���� ����� �� �������� ������� ����� ������ ������������ ������, ������������ �������� ��� ��������� ��������.
	//   ��� ���������� ��������� ��� ���������
	CTelemetrySocket* ClientST = new CTelemetrySocket("localhost", "8888", DataRec);
	
	// ���������, ��� �� ������
	if(ST->ErrorCode==0 && ClientST->ErrorCode==0)
	{
		// ���������� �������� ������
		ClientST->Send("���� - TEST");
		
		//ST->Send(1, "OK");
	}

	// �� ��� ��������� ����������� ���� �� ������� ������
	// � ������� ������� ��������� �� ���������� �������
	while(!end)
	{
		Sleep(1000);
	}

	// ������� �������
	delete ClientST;
	delete ST;

	return 0;
}
#include "Telemetry.h"
#include <process.h>
#include <sys/timeb.h>
#include <sstream>
#include "Utility.h"

// �������� ���������� ������ � �������������� �������� ������
CTelemetrySocket::CTelemetrySocket(char* port, //int bufferSize, 
		void (*ConnectionRecieved)(CSocketTransfer* ST, int ConnIdentifier), 
		void (*DataReceived)(CSocketTransfer* ST, int ConnIdentifier, char* Data), 
		void (*ConnectionClosed)(CSocketTransfer* ST, int ConnIdentifier))
		// ����� �������������� ������� �����, �� ��������� ������ �� ��, ��� ���������� � ���� ������ 
		//     ��� ����������� ��������� �������, ��������������� ����� ������.
		//     ��������� ������ ����� �������������� � ������� �������, ��������� ��� ������������� �������
		: CSocketTransfer(port, 1500, _ConnectionRecieved, _DataReceived, _ConnectionClosed) 
{
	// ��������� ������
	ConnRecieved = ConnectionRecieved;
	DataReceived_S = DataReceived;
	DataReceived_C = nullptr;
	ConnClosed = ConnectionClosed;

	// ��� ���������� ������� ������ ����� ������ ������ �� ���������.
	// ��������� � ������ ���������, �� ��������� Is_Online=false ����� �� ������������ �����������
	TelemetryValue TV;
	TV.Is_Online=false;
	TV.Telemetry_IN = -1;
	TV.Telemetry_OUT = -1;

	TelemetryMas.push_back(TV);

	// ��������� �������, ������� ��� � ������ ����� ��������� ������� �������� �����������
	_beginthread(TelemetryWorker,0,this);
}

// ���������� ��� ����������� �������
CTelemetrySocket::CTelemetrySocket(char* host, char* port, //int bufferSize, 
	void (*DataReceived)(CSocketTransfer* ST, char* Data))
	// �������������� ������� �����, �� ��������� ����� ��������� ������
	: CSocketTransfer(host, port, 1500, _DataReceived)
{
	// ��������� �������� �����
	ConnRecieved = nullptr;
	DataReceived_S = nullptr;
	DataReceived_C = DataReceived;
	ConnClosed = nullptr;

	// ��������� ������ ��� ���������� ������� �������� ���������
	TelemetryValue TV;
	TV.Is_Online=true;
	TV.Telemetry_IN = -1;
	TV.Telemetry_OUT = -1;

	TelemetryMas.push_back(TV);
}

CTelemetrySocket::~CTelemetrySocket(void)
{
}

// ����� ��� ��������� ������ �����������
void CTelemetrySocket::_ConnectionRecieved(CSocketTransfer* ST, int ConnIdentifier)
{
	// ��� ������� ����������� ��������� � ������ ������, ������� ����� ��������� ������ � ���������
	TelemetryValue TV;
	TV.Is_Online=true;
	TV.Telemetry_IN = -1;
	TV.Telemetry_OUT = -1;

	((CTelemetrySocket*)ST)->TelemetryMas.push_back(TV);

	// ���� ��� ����� ���� ���������, �������� ���
	if(((CTelemetrySocket*)ST)->ConnRecieved != nullptr)
		((CTelemetrySocket*)ST)->ConnRecieved(ST, ConnIdentifier);
}

// ����� ��� ��������� ��������� ������ �� ������� �������
void CTelemetrySocket::_DataReceived(CSocketTransfer* ST, int ConnIdentifier, char* Data)
{
	// ���� �������� �������� ��� ���������, �� ����������� ��� �����
	// � �� �������� ����� ��� ��������� ���������� ������
	if(strlen(Data)>11 && strncmp(Data,"<telemetry>",11)==0)
	{
		// �������� �������� �������, ������������ � ������, � ���������� Time
		char* delimiter = strchr(Data, '\\');
		int timestrSize = delimiter-Data-11;
		char* Time = new char[timestrSize+1];
		memcpy(Time, &Data[11], timestrSize);
		Time[timestrSize]=0;
		
		// ��������� � double
		std::string fs(Time);
		double f = std::stod(fs);

		// �������� ������� �������� ������� � ������� ���������� ������ � 01.01.1970
		double CurTime = CUtility::GetCurTime();

		// �������� �������
		double R = CurTime - f;

		// ��������� ��������� ���������� ���������� ������ � ���������� �������
		((CTelemetrySocket*)ST)->TelemetryMas[ConnIdentifier].Telemetry_IN = (int)(strlen(Data) / R);

		// ���������� ������� ������� ��� ������ �� ������� �������
		std::stringstream t_in;
		t_in << "telemetry_result:" << ((CTelemetrySocket*)ST)->TelemetryMas[ConnIdentifier].Telemetry_IN;

		ST->Send(ConnIdentifier, t_in.str().c_str());

		delete[] Time;
	}
	else if(strlen(Data)>20 && strncmp(Data,"telemetry_result:",17)==0)
	{
		// �� ������� ��������� ������� ������� - ���������� � � ��������������� ������
		std::string is(&Data[17]);
		((CTelemetrySocket*)ST)->TelemetryMas[ConnIdentifier].Telemetry_OUT = std::stoi(is);
	}
	else if(((CTelemetrySocket*)ST)->DataReceived_S != nullptr)
	{
		// ��������� ���������� ������ ������� ������, ���������� ��� ������������� �������
		((CTelemetrySocket*)ST)->DataReceived_S(ST, ConnIdentifier, Data);
	}
}

// ����� ��� ��������� ��������� ������ �� ������� �������
void CTelemetrySocket::_DataReceived(CSocketTransfer* ST, char* Data)
{
	// ���� �������� �������� ��� ���������, �� ����������� ��� �����
	// � �� �������� ����� ��� ��������� ���������� ������
	if(strlen(Data)>11 && strncmp(Data,"<telemetry>",11)==0)
	{
		// �������� �������� �������, ������������ � ������, � ���������� Time
		char* delimiter = strchr(Data, '\\');
		int timestrSize = delimiter-Data-11;
		char* Time = new char[timestrSize+1];
		memcpy(Time, &Data[11], timestrSize);
		Time[timestrSize]=0;
		
		// ��������� � double
		std::string fs(Time);
		double f = std::stod(fs);

		// �������� ������� �������� ������� � ������� ���������� ������ � 01.01.1970
		double CurTime = CUtility::GetCurTime();

		// �������� �������
		double R = CurTime - f;

		// ��������� ��������� ���������� ���������� ������ � ���������� �������
		((CTelemetrySocket*)ST)->TelemetryMas[0].Telemetry_IN = (long)(strlen(Data) / R);

		// ���������� ����� �� ����� � ����� �������� ������� ��� ��������� �������� �������� � ������� �������
		std::stringstream ss;
		ss<<"<telemetry>"<<CUtility::getTimeString()<<delimiter;

		ST->Send(ss.str().c_str());

		// �� ������ ������ ������� �������
		Sleep(500);

		// ���������� ������� ������� ��� ������ �� ������� �������
		std::stringstream t_in;
		t_in << "telemetry_result:" << ((CTelemetrySocket*)ST)->TelemetryMas[0].Telemetry_IN;

		ST->Send(t_in.str().c_str());

		delete[] Time;
	}
	else if(strlen(Data)>20 && strncmp(Data,"telemetry_result:",17)==0)
	{
		// �� ������� ��������� ������� ������� - ���������� � � ��������������� ������
		std::string is(&Data[17]);
		((CTelemetrySocket*)ST)->TelemetryMas[0].Telemetry_OUT = std::stoi(is);
	}
	else if(((CTelemetrySocket*)ST)->DataReceived_C != nullptr)
	{
		// ��������� ���������� ������ ������� ������, ���������� ��� ������������� �������
		((CTelemetrySocket*)ST)->DataReceived_C(ST, Data);
	}
}

// ����� ��� ��������� ������� �������� ����������
void CTelemetrySocket::_ConnectionClosed(CSocketTransfer* ST, int ConnIdentifier)
{
	((CTelemetrySocket*)ST)->TelemetryMas[ConnIdentifier].Is_Online = false; // �������� ����� �����������

	// ���� ������� ����� ��� �������������, �������� ���
	if(((CTelemetrySocket*)ST)->ConnClosed != nullptr)
		((CTelemetrySocket*)ST)->ConnClosed(ST, ConnIdentifier);
}

// ����� ��� �������������� ������ ��������� �������� ��������
// �������� ������ ������ �� ������� �������
void CTelemetrySocket::TelemetryWorker(void* CTS)
{
	// ������������� ������
	CTelemetrySocket* TS = (CTelemetrySocket*)CTS;
	
	int length = 1048500;

	// ���������� ������ �������� �����
	char* RandomString = new char[length];
	CUtility::gen_random(RandomString, length);	

	while(true)
	{
		for(size_t i=0;i<TS->TelemetryMas.size();i++)
		{
			// ���� ������ ��� ��� ��������, ��������� � ����������
			if(TS->TelemetryMas[i].Is_Online == false)
				continue;

			// ���������� ������, ���������� ������� �����
			std::stringstream ss;
			ss<<"<telemetry>"<<CUtility::getTimeString()<<"\\"<<RandomString<<"</telemetry>";

			// ���������� � �������
			TS->Send(i, ss.str().c_str());

			// ������ ����� ����� ���������� ��� ���� ����� �� �������� �����
			Sleep(5000);
		}

		// ����� �������� ������ �������.
		// ����� ���� ����� ��� ��������� ��������� ���������� ������
		for(int i=0;i<30;i++)
		{
			Sleep(1000);
		}
	}
	_endthread();
}

// ���������� ������ � ������� ��������� �� ���������� �������
TelemetryValue CTelemetrySocket::GiveTelemetry(int ConnIdentifier)
{
	return TelemetryMas[ConnIdentifier];
}

// ���������� ������ � ������� ��������� ��� �������
TelemetryValue CTelemetrySocket::GiveTelemetry()
{
	return TelemetryMas[0];
}

#include "Utility.h"
#include <stdlib.h>
#include <sys/timeb.h>
#include <sstream>
#include <algorithm>
#include <atlconv.h>
#include <strsafe.h>
#define SECURITY_WIN32
//#include <security.h>
//#include <schannel.h>
//#include <cryptuiapi.h>

//CUtility::CUtility(void)
//{
//}
//
//
//CUtility::~CUtility(void)
//{
//}

// ��������� ��������� ������ �������� �����
void CUtility::gen_random(char *s, const int len) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    s[len] = 0;
}

// ��������� ������ � ������� �������� � ����������� �������
std::string CUtility::getTimeString()
{
	_timeb tstruct;
	_ftime_s(&tstruct);

	std::stringstream ss;

	ss<<(tstruct.timezone*60 + tstruct.time)<<"."<<tstruct.millitm;

	return ss.str();
}

// ��������� �������� �������
//   � ������� ���������� ������ � �������������� � 01.01.1970
double CUtility::GetCurTime()
{
	_timeb tstruct;
	_ftime_s(&tstruct);

	return (tstruct.timezone*60.0 + tstruct.time) + tstruct.millitm/1000.0;
}

// ��������, �������� �� ������������ ������� �� ������� ���������� / �������
// �� ������� ������ ���� ����� �� ������������
bool IsUserAdmin()
{
	BOOL b;
	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	PSID AdministratorsGroup;
	b = AllocateAndInitializeSid(
		&NtAuthority,
		2,
		SECURITY_BUILTIN_DOMAIN_RID,
		DOMAIN_ALIAS_RID_ADMINS,
		0, 0, 0, 0, 0, 0,
		&AdministratorsGroup);
	if (b)
	{
		if (!CheckTokenMembership(NULL, AdministratorsGroup, &b))
		{
			b = FALSE;
		}
		FreeSid(AdministratorsGroup);
	}

	return (b == TRUE);
}

/*SECURITY_STATUS SelectServerCert(PCCERT_CONTEXT& pCertContext, LPCTSTR pszSubjectName)
{
   SECURITY_STATUS status = CertFindServerByName(pCertContext, pszSubjectName); // Add "true" to look in user store, "false", or nothing looks in machine store

   return status;
}

SECURITY_STATUS CertFindServerByName(PCCERT_CONTEXT & pCertContext, LPCTSTR pszSubjectName, boolean fUserStore)
{
   HCERTSTORE  hMyCertStore = NULL;
   TCHAR pszFriendlyNameString[128];
   TCHAR	pszNameString[128];

   if (pszSubjectName == NULL || _tcslen(pszSubjectName) == 0)
   {
      DebugMsg("**** No subject name specified!");
      return E_POINTER;
   }

   if (fUserStore)
      hMyCertStore = CertOpenSystemStore(NULL, _T("MY"));
   else
   {	// Open the local machine certificate store.
      hMyCertStore = CertOpenStore(CERT_STORE_PROV_SYSTEM,
         X509_ASN_ENCODING,
         NULL,
         CERT_STORE_OPEN_EXISTING_FLAG | CERT_STORE_READONLY_FLAG | CERT_SYSTEM_STORE_LOCAL_MACHINE,
         L"MY");
   }

   if (!hMyCertStore)
   {
      int err = GetLastError();

      if (err == ERROR_ACCESS_DENIED)
         DebugMsg("**** CertOpenStore failed with 'access denied'");
      else
         DebugMsg("**** Error %d returned by CertOpenStore", err);
      return HRESULT_FROM_WIN32(err);
   }

   if (pCertContext)	// The caller passed in a certificate context we no longer need, so free it
      CertFreeCertificateContext(pCertContext);
   pCertContext = NULL;

   char * serverauth = szOID_PKIX_KP_SERVER_AUTH;
   CERT_ENHKEY_USAGE eku;
   PCCERT_CONTEXT pCertContextSaved = NULL;
   eku.cUsageIdentifier = 1;
   eku.rgpszUsageIdentifier = &serverauth;
   // Find a server certificate. Note that this code just searches for a 
   // certificate that has the required enhanced key usage for server authentication
   // it then selects the best one (ideally one that contains the server name
   // in the subject name).

   while (NULL != (pCertContext = CertFindCertificateInStore(hMyCertStore,
      X509_ASN_ENCODING,
      CERT_FIND_OPTIONAL_ENHKEY_USAGE_FLAG,
      CERT_FIND_ENHKEY_USAGE,
      &eku,
      pCertContext)))
   {
      //ShowCertInfo(pCertContext);
      if (!CertGetNameString(pCertContext, CERT_NAME_FRIENDLY_DISPLAY_TYPE, 0, NULL, pszFriendlyNameString, sizeof(pszFriendlyNameString)))
      {
         DebugMsg("CertGetNameString failed getting friendly name.");
         continue;
      }
      DebugMsg("Certificate '%S' is allowed to be used for server authentication.", ATL::CT2W(pszFriendlyNameString));
      if (!CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, sizeof(pszNameString)))
         DebugMsg("CertGetNameString failed getting subject name.");
      else if (!MatchCertHostName(pCertContext, pszSubjectName))  //  (_tcscmp(pszNameString, pszSubjectName))
         DebugMsg("Certificate has wrong subject name.");
      else if (CertCompareCertificateName(X509_ASN_ENCODING, &pCertContext->pCertInfo->Subject, &pCertContext->pCertInfo->Issuer))
      {
         if (!pCertContextSaved)
         {
            DebugMsg("A self-signed certificate was found and saved in case it is needed.");
            pCertContextSaved = CertDuplicateCertificateContext(pCertContext);
         }
      }
      else
      {
         DebugMsg("Certificate is acceptable.");
         if (pCertContextSaved)	// We have a saved self signed certificate context we no longer need, so free it
            CertFreeCertificateContext(pCertContextSaved);
         pCertContextSaved = NULL;
         break;
      }
   }

   if (pCertContextSaved && !pCertContext)
   {	// We have a saved self-signed certificate and nothing better 
      DebugMsg("A self-signed certificate was the best we had.");
      pCertContext = pCertContextSaved;
      pCertContextSaved = NULL;
   }

   if (!pCertContext)
   {
      DWORD LastError = GetLastError();
      if (LastError == CRYPT_E_NOT_FOUND)
      {
         DebugMsg("**** CertFindCertificateInStore did not find a certificate, creating one");
         pCertContext = CreateCertificate(true, pszSubjectName);
         if (!pCertContext)
         {
            LastError = GetLastError();
            DebugMsg("**** Error 0x%x returned by CreateCertificate", LastError);
            std::cout << "Could not create certificate, are you running as administrator?" << std::endl;
            return HRESULT_FROM_WIN32(LastError);
         }
      }
      else
      {
         DebugMsg("**** Error 0x%x returned by CertFindCertificateInStore", LastError);
         return HRESULT_FROM_WIN32(LastError);
      }
   }

   return SEC_E_OK;
}*/

//char* CUtility::XmlEncode(char* str)
//{
//	std::string FinStr;
//
//	while(*str!='\0')
//	{
//		switch (*str)
//		{
//		case '<':
//			{
//				FinStr+="&lt";
//			}
//			break;
//		case '>':
//			{
//				FinStr+="&gt";
//			}
//			break;
//		default:
//			{
//				FinStr+=*str;
//			}
//			break;
//		}
//		str++;
//	}
//	
//	char* newStr = new char[FinStr.length()+1];
//	newStr[FinStr.length()]=0;
//	strcpy_s(newStr, FinStr.length(), FinStr.c_str());
//
//	return newStr;
//}


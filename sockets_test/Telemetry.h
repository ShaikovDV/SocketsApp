#pragma once
#include "SocketTransfer.h"
#include <deque>

// ������ ��� ���������� ������ ���������
struct TelemetryValue
{
	long Telemetry_IN;
	long Telemetry_OUT;
	bool Is_Online;
};

// ����� ��� �������� ������ ����� ������ � �������������� ������������� ���������� �������� �������� ����������
// ��������� �������� ���������� �� CSocketTransfer
class CTelemetrySocket : public CSocketTransfer
{
public:
	// ����������� ���������� �������
	CTelemetrySocket(char* port, void (*ConnectionRecieved)(CSocketTransfer* ST, int ConnIdentifier), 
		void (*DataReceived)(CSocketTransfer* ST, int ConnIdentifier, char* Data), 
		void (*ConnectionClosed)(CSocketTransfer* ST, int ConnIdentifier));

	// ���������� ����������� �������
	CTelemetrySocket(char* host, char* port, void (*DataReceived)(CSocketTransfer* ST, char* Data));

	~CTelemetrySocket(void);

	// ����� ��� ��������� ������ ��������� �������� ��� ���������� �������
	TelemetryValue GiveTelemetry(int ConnIdentifier);
	// ����� ��� ��������� ������ ��������� ��������
	TelemetryValue GiveTelemetry();

private:

	// ������ ��� �������� ���������
	std::deque<TelemetryValue> TelemetryMas;

	// ����� ��� ������� ������������� ��������� ��������
	static void TelemetryWorker(void* TS);

	// ������� ��� ���������� �������� ������ �� ������ ��������� �������
	void (*ConnRecieved)(CSocketTransfer* ST, int ConnIdentifier);
	void (*DataReceived_S)(CSocketTransfer* ST, int ConnIdentifier, char* Data);
	void (*DataReceived_C)(CSocketTransfer* ST, char* Data);
	void (*ConnClosed)(CSocketTransfer* ST, int ConnIdentifier);

	// ������ ��������� �������, ������ ����� ����������� � ������ �������.
	// ���� ����������, ������ ����� �������� ���������� ������ �� ����������� �������.
	static void _ConnectionRecieved(CSocketTransfer* ST, int ConnIdentifier);
	static void _DataReceived(CSocketTransfer* ST, int ConnIdentifier, char* Data);
	static void _DataReceived(CSocketTransfer* ST, char* Data);
	static void _ConnectionClosed(CSocketTransfer* ST, int ConnIdentifier);

};

